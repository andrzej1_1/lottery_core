use async_trait::async_trait;
use wamp_async::WampId;

use crate::error::CoreError;

/// Type of publishing result.
pub type PublishRes = Option<WampId>;

/// Helper for WAMP client to use it much easier.
#[async_trait]
pub trait WampHelper {
  /// Publishes kwargs in given topic.
  /// In case of WAMP issue `CoreError` is returned.
  async fn pub_kw(&self, topic: &str, kwargs: serde_json::Value) -> Result<PublishRes, CoreError>;
}

#[async_trait]
impl WampHelper for wamp_async::Client<'_> {
  /// Publishes kwargs in given topic.
  /// In case of WAMP issue `CoreError` is returned.
  ///
  /// WARNING: Invalid json data will NOT be reported, but replaced with NULL.
  async fn pub_kw(&self, topic: &str, kwargs: serde_json::Value) -> Result<PublishRes, CoreError> {
    let kwargs = kwargs.as_object().cloned();
    let res = self.publish(topic, None, kwargs, true).await?;
    Ok(res)
  }
}
