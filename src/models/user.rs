use redis_orm::active_record::ActiveRecord;
use redis_orm::stringmap::StringMap;
use redis_orm::RedisORM;
use serde::{Deserialize, Serialize};

/// Type used to represent User ID.
pub type UserId = u32;

/// Model that represents user account.
#[derive(Debug, Default, Serialize, Deserialize, RedisORM)]
pub struct User {
  /// User ID.
  id: Option<i64>,
  /// User's browser fingeprint.
  fingerprint: String,
  /// Balance of user wallet.
  wallet: u64,
}

impl User {
  /// Constructs new User for given fingerprint.
  /// By default we set user balance 2000.
  pub fn new(fingerprint: &str) -> Self {
    Self {
      id: None,
      fingerprint: fingerprint.to_string(),
      wallet: 2000,
    }
  }

  /// Gets `id` property.
  pub fn id(&self) -> &Option<i64> {
    &self.id
  }

  /// Gets `fingerprint` property.
  pub fn fingerprint(&self) -> &String {
    &self.fingerprint
  }

  /// Gets `wallet` property.
  pub fn wallet(&self) -> u64 {
    self.wallet
  }

  /// Increases wallet balance by given amount.
  ///
  /// NOTE: You should use mutex for performing this
  /// kind of financial operations.
  pub fn add_to_wallet(&mut self, amount: u32) {
    self.wallet += amount as u64;
  }

  /// Decreases wallet balance by given amount.
  /// In case there is not enough funds, the wallet is set to 0.
  ///
  /// NOTE: You should use mutex for performing this
  /// kind of financial operations.
  pub fn remove_from_wallet(&mut self, amount: u32) {
    if amount as u64 > self.wallet {
      eprintln!("Negative balance detected, falling back to 0.");
      self.wallet = 0;
      return;
    }
    self.wallet -= amount as u64;
  }
}
