use rand::{distributions::Alphanumeric, Rng};
use redis_orm::active_record::ActiveRecord;
use redis_orm::stringmap::StringMap;
use redis_orm::RedisORM;
use serde::{Deserialize, Serialize};
use sha2::{Digest, Sha256};

use crate::error::CoreError;
use crate::models::LotteryRound;

/// Number of round per seed.
/// After we reach it, new seed should be generated.
const ROUNDS_PER_SEED: u32 = 60 * 24;

/// Model that represents lottery secret (seed).
#[derive(Debug, Default, Serialize, Deserialize, Clone, RedisORM)]
pub struct Seed {
  // Seed ID.
  id: Option<i64>,
  // Whether key should be visible to users.
  is_visible: bool,
  // Secret key (seed).
  key: String,
  // Sha256 of secret key.
  hash: String, // sha256 of key
}

impl Seed {
  /// Constructs new Seed with random key/secret inside.
  pub fn new() -> Self {
    // Generate random key and format it to hex digest.
    let secret: String = rand::thread_rng()
      .sample_iter(&Alphanumeric)
      .take(32)
      .map(char::from)
      .collect();
    let hash = format!("{:x}", Sha256::digest(secret.as_bytes()));

    // Create Seed.
    Self {
      id: None,
      is_visible: false,
      key: secret,
      hash,
    }
  }

  /// Sets secret/key to empty value.
  /// Should be used to hide key before end user.
  ///
  /// WARNING: Do not save model after you use this
  /// function, otherwise key will be lost forever.
  pub fn hide_key(&mut self) {
    self.key = "".to_owned();
  }

  /// Enables visibility of seed key.
  pub fn make_visible(&mut self) {
    self.is_visible = true;
  }

  /// Gets `key` property.
  pub fn key(&self) -> &String {
    &self.key
  }

  /// Gets `id` property.
  pub fn id(&self) -> &Option<i64> {
    &self.id
  }

  /// Gets `is_visible` property.
  pub fn is_visible(&self) -> bool {
    self.is_visible
  }

  /// Checks wheter it is time to generate new seed,
  /// basing on number of total rounds played.
  ///
  /// # Error
  /// In case of database issue `CoreError` is returned.
  pub async fn is_regeneration_needed(con: &mut redis::aio::Connection) -> Result<bool, CoreError> {
    // Count number of total round played.
    // No regeneration applies if no round played at all.
    let total_rounds = LotteryRound::db_count_all(con).await?;
    if total_rounds == 0 {
      return Ok(false);
    }

    // After exactly N rounds we want to regenerate seed.
    if total_rounds % ROUNDS_PER_SEED == 0 {
      return Ok(true);
    }

    Ok(false)
  }
}
