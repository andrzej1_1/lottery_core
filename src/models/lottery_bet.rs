use redis_orm::active_record::ActiveRecord;
use redis_orm::stringmap::StringMap;
use redis_orm::RedisORM;
use serde::{Deserialize, Serialize};

use std::collections::HashSet;

use crate::error::CoreError;
use crate::models::UserId;

/// Model that represents lottery ticket with 6 numbers selected.
#[derive(Debug, Default, Serialize, Deserialize, RedisORM)]
pub struct LotteryBet {
  /// Bet ID.
  id: Option<i64>,
  /// Selected numbers.
  numbers: Vec<u8>,
  /// User that placed the bet.
  user_id: UserId,
  /// Round to which bet is assigned.
  round_id: i64,
  /// Whether ticket was processed (result accounted).
  is_accounted: bool,
}

impl LotteryBet {
  /// Constructs new bet for given round, user and numbers.
  ///
  /// # Error
  /// If numbers are invalid `CoreError` is returned.
  pub fn new(round_id: i64, user_id: UserId, numbers: Vec<u8>) -> Result<Self, CoreError> {
    // Validate numbers.
    if !LotteryBet::are_numbers_valid(&numbers) {
      return Err(CoreError::InvalidInput("Invalid numbers".into()));
    }

    // Create bet.
    Ok(Self {
      id: None,
      numbers,
      user_id,
      round_id,
      is_accounted: false,
    })
  }

  /// Validates if given numbers are valid.
  /// There must be exactly 6 unique numbers, all within <1, 49>.
  pub fn are_numbers_valid(numbers: &[u8]) -> bool {
    // Check if numbers are within valid range and build set from them.
    let mut numbers_map = HashSet::new();
    for number in numbers.iter() {
      if !(&1..=&49).contains(&number) {
        return false;
      }
      numbers_map.insert(number);
    }

    // Make sure set conains exactly 6 numbers.
    if numbers_map.len() != 6 {
      return false;
    }

    true
  }

  /// Gets `id` property.
  pub fn id(&self) -> &Option<i64> {
    &self.id
  }

  /// Gets `user_id` property.
  pub fn user_id(&self) -> UserId {
    self.user_id
  }

  /// Gets `round_id` property.
  pub fn round_id(&self) -> i64 {
    self.round_id
  }

  /// Gets `numbers` property.
  pub fn numbers(&self) -> &Vec<u8> {
    &self.numbers
  }

  /// Sets current bet as accounted.
  pub fn mark_accounted(&mut self) {
    self.is_accounted = true;
  }
}
