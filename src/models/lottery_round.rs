use redis_orm::active_record::ActiveRecord;
use redis_orm::stringmap::StringMap;
use redis_orm::RedisORM;
use serde::{Deserialize, Serialize};

/// Model that represents single lottery round.
#[derive(Debug, Default, Serialize, Deserialize, RedisORM)]
pub struct LotteryRound {
  /// Round ID.
  id: Option<i64>,
  /// Seed assigned to this round.
  seed_id: i64,
}

impl LotteryRound {
  /// Constructs new round for given seed.
  pub fn new(seed_id: i64) -> Self {
    Self { id: None, seed_id }
  }

  /// Gets `id` property.
  pub fn id(&self) -> &Option<i64> {
    &self.id
  }

  /// Gets `seed_id` property.
  pub fn seed_id(&self) -> i64 {
    self.seed_id
  }
}
