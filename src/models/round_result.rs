use rand::{Rng, SeedableRng};
use rand_chacha::ChaCha20Rng;
use redis_orm::active_record::ActiveRecord;
use redis_orm::stringmap::StringMap;
use redis_orm::RedisORM;
use serde::{Deserialize, Serialize};
use sha2::{Digest, Sha256};

use std::collections::HashSet;

/// Model that represents lottery round result.
#[derive(Debug, Default, Serialize, Deserialize, RedisORM)]
pub struct RoundResult {
  /// Result ID.
  id: Option<i64>,
  /// Round which is connected to this result.
  round_id: i64,
  /// Generated winning numbers.
  numbers: [u8; 6],
  /// Timestamp of round start.
  timestamp: i64,
}

impl RoundResult {
  /// Constructs new result for given seed, round and round time.
  pub fn new(secret: &str, round_id: i64, timestamp: i64) -> Self {
    // Generate winning numbers.
    let numbers = RoundResult::generate_winning_nums(secret, round_id);

    // Create Result.
    Self {
      id: None,
      round_id,
      numbers,
      timestamp,
    }
  }

  // Gets `numbers` property.
  pub fn numbers(&self) -> &[u8; 6] {
    &self.numbers
  }

  // Generates winning numbers for given seed and round.
  // Output of this function is deterministic: it generates same output
  // for the same input.
  //
  // # Algorithm
  // - Use ChaCha20 RNG and initialize with sha256(seed + "-" + round_id).
  // - Use 0 as nonce.
  // - Generate numbers in <1..49> untill we have 6 unique.
  fn generate_winning_nums(secret: &str, round_id: i64) -> [u8; 6] {
    // Create and initialize RNG.
    let seed = format!("{}-{}", secret, round_id);
    let seed: [u8; 32] = *Sha256::digest(seed.as_bytes()).as_mut();
    let mut rng = ChaCha20Rng::from_seed(seed);

    // Set nonce to 0 explicitly.
    rng.set_stream(0);

    // Generate winning numbers.
    let mut numbers: [u8; 6] = [0; 6];
    let mut numbers_map: HashSet<u8> = HashSet::new();
    while numbers_map.len() < 6 {
      let num: u8 = rng.gen_range(0..49) + 1;
      numbers[numbers_map.len()] = num;
      numbers_map.insert(num);
    }

    numbers
  }
}
