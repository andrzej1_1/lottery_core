pub mod models;

pub mod error;

pub mod helpers;

use chrono::{DateTime, Timelike, Utc};
use deadpool_redis::Connection;
use redis_orm::{
  active_query::{Filter, QuerySort, RedisVal},
  active_record::ActiveRecord,
  mutex::Mutex,
};
use serde_json::json;
use tokio::time::Interval;
use wamp_async::Client;

use std::collections::HashMap;

use crate::error::CoreError;
use crate::helpers::WampHelper;
use crate::models::{LotteryBet, LotteryRound, RoundResult, Seed, User};

/// Engine for running lottery.
pub struct LotteryCore {
  /// Interval for running rounds periodically.
  interval: Interval,
}

impl LotteryCore {
  /// Constructs new LotteryCore with full minute interval.
  /// Missed ticks are set to be skipped - see https://docs.rs/tokio/latest/tokio/time/enum.MissedTickBehavior.html#variant.Skip.
  pub fn new() -> Self {
    // Create full minute interval and set missed tick behavior.
    let mut interval = Self::get_full_minute_interval();
    interval.set_missed_tick_behavior(tokio::time::MissedTickBehavior::Skip);

    // Create LotteryCore.
    Self { interval }
  }

  /// Creates full minute interval.
  /// By full minute we mean time with exactly 0 seconds,
  /// e.g. 05:35:00, 05:36:00, etc.
  fn get_full_minute_interval() -> Interval {
    // Get current time, then reset seconds part and compute difference.
    let now = chrono::Utc::now();
    let mut start = now
      .date()
      .and_hms(now.hour(), now.minute(), 0)
      .signed_duration_since(now);
    if start < chrono::Duration::seconds(0) {
      start = start.checked_add(&chrono::Duration::minutes(1)).unwrap();
    }

    // Set interval period to 1 minute.
    let period = chrono::Duration::minutes(1).to_std().unwrap();

    // Create interval with time (now + computed difference) and period.
    tokio::time::interval_at(
      tokio::time::Instant::now() + start.to_std().unwrap(),
      period,
    )
  }

  /// Initializes lottery:
  /// 1. Makes sure there is some round and seed, otherwise creates them.
  /// 2. Processes all unaccounted bets (from previous runs).
  ///
  /// # Error
  /// In case of database issue or processing error `CoreError` is returned.
  pub async fn initialize(
    &self,
    redis_con: &mut Connection,
    wamp_client: &Client<'static>,
  ) -> Result<(), CoreError> {
    // Make sure that current round exists.
    // If not then just create new seed and round connected with it.
    let round_res = self.get_current_round(redis_con).await;
    let round_pk = match round_res {
      Ok((_round, round_pk)) => round_pk,
      Err(CoreError::MissingData(_e)) => {
        let (_seed, seed_pk) = Seed::new().db_save(redis_con).await?;
        let (_round, round_pk) = LotteryRound::new(seed_pk).db_save(redis_con).await?;
        round_pk
      }
      Err(e) => return Err(e),
    };

    // Process remaining unaccounted bets in blocking way.
    let bets = self.get_unaccounted_bets(round_pk, redis_con).await?;
    self.process_bets(bets, redis_con, wamp_client).await?;

    Ok(())
  }

  /// Waits until full minute interval is reached and returns current time.
  pub async fn wait_until_ready(&mut self) -> DateTime<Utc> {
    // Wait for tick.
    self.interval.tick().await;

    // Return curret time.
    chrono::Utc::now()
  }

  /// Runs single lottery round:
  /// - Processes bets from current round, generates new round,
  /// - Generates new seed if necessary, reveals previous one,
  /// - Notifies WAMP about new round played.
  ///
  /// # Error
  /// In case of database/wamp issue or processing error `CoreError` is returned.
  pub async fn run_round(
    &self,
    round_time: DateTime<Utc>,
    wamp_client: &Client<'static>,
    mut redis_con: Connection,
  ) -> Result<(), CoreError> {
    println!("= New Round =");

    // Firstly get current round and its seed.
    let (round, round_id) = self.get_current_round(&mut redis_con).await?;
    let seed_id = round.seed_id();
    let (seed, seed_pk) = self.get_seed(seed_id, &mut redis_con).await?;

    // Check if current seed can be used for next round or we should generate new one.
    // Determine seed for next round.
    let new_seed_necessary = Seed::is_regeneration_needed(&mut redis_con).await?;
    let (_next_seed, next_seed_pk) = match new_seed_necessary {
      true => Seed::new().db_save(&mut redis_con).await?,
      false => (seed.clone(), seed_pk),
    };

    // Create new round for future bets.
    let (_next_round, _next_round_pk) = self
      .generate_next_round(next_seed_pk, &mut redis_con)
      .await?;

    // Now current round is closed, so we can get all unaccounted bets.
    let unaccounted_bets = self.get_unaccounted_bets(round_id, &mut redis_con).await?;

    // Generate round result.
    let round_result = RoundResult::new(seed.key(), round_id, round_time.timestamp());
    let (round_result, _pk) = round_result.db_save(&mut redis_con).await?;
    let winning_numbers = round_result.numbers();

    // Reveal current seed if relevant.
    if new_seed_necessary {
      self.reveal_seed(seed, &mut redis_con).await?;
    }

    // Notify router about newly finished round.
    let topic = "com.lottery.new_round";
    let kwargs = json!({
      "winning_numbers": winning_numbers,
      "round_id": round_id
    });
    wamp_client.pub_kw(topic, kwargs).await?;

    // Process users' bets.
    self
      .process_bets(unaccounted_bets, &mut redis_con, wamp_client)
      .await?;

    Ok(())
  }

  /// Compute prize for typed numbers, compared to winning ones.
  /// We decided to award user with 100 for typing at 2 numbers correctly,
  /// and each next correct number is mulitpling award by 10.
  /// So for 6 correct numbers the award will be 1,000,000.
  fn get_prize(winning_nums: &[u8; 6], user_nums: &[u8]) -> (u64, u8) {
    // Count correct numbers.
    let mut correct_nums: u8 = 0;
    for win_num in winning_nums {
      if user_nums.iter().any(|&x| &x == win_num) {
        correct_nums += 1;
      }
    }

    // Compute award if at least 2 numbers correct.
    if correct_nums >= 2 {
      return (10_u64.pow(correct_nums.into()), correct_nums);
    }

    // No award otherwise.
    (0, correct_nums)
  }

  /// Processes given bet:
  /// - Updates user wallet with award if relevant,
  /// - Notifies about award and wallet balance update,
  /// - Marks bet as processed (accounted).
  ///
  /// # Error
  /// In case of database/wamp issue or processing error `CoreError` is returned.
  async fn process_bet(
    &self,
    mut bet: LotteryBet,
    bet_id: i64,
    winning_nums: &[u8; 6],
    redis_con: &mut Connection,
    wamp_client: &Client<'static>,
  ) -> Result<(), CoreError> {
    // Compute prize.
    let (prize, correct_nums) = Self::get_prize(winning_nums, bet.numbers());
    let user_id = bet.user_id();
    println!(
      "User {:?} typed {} numbers correcly and won {}",
      user_id, correct_nums, prize
    );

    // Update user wallet.
    // NOTE: Operation is protected with mutex named "User".
    let mutex_factory = Mutex::new();
    let guard = Mutex::new().lock("User".into(), redis_con).await?;
    let (mut user, mut _user_pk) = User::find()
      .filter(Filter::Condition(
        "id".into(),
        RedisVal::Numeric(user_id as i64),
      ))
      .one::<User>(redis_con)
      .await?
      .ok_or_else(|| CoreError::MissingData("Unable to find user.".into()))?;
    if prize > 0 {
      user.add_to_wallet(prize as u32);
      user = user.db_save(redis_con).await?.0;
    }
    mutex_factory.unlock(&guard, redis_con).await?;

    // Send notification about prize.
    let user_fp = user.fingerprint();
    let topic = format!("com.lottery.user_{user_fp}.update");
    let kwargs = json!({
      "correct_nums": correct_nums,
      "prize": prize,
      "ticket_id": bet_id,
    });
    wamp_client.pub_kw(topic.as_str(), kwargs).await?;

    // Send notification about new wallet state.
    let topic = format!("com.lottery.user_{user_fp}.wallet");
    let kwargs = json!({
      "balance": user.wallet(),
      "action": "prize",
    });
    wamp_client.pub_kw(topic.as_str(), kwargs).await?;

    // Mark bet as processed.
    bet.mark_accounted();
    bet.db_save(redis_con).await?;

    Ok(())
  }

  /// Gets current lottery round.
  ///
  /// # Error
  /// In case of database issue `CoreError` is returned.
  async fn get_current_round(
    &self,
    redis_con: &mut Connection,
  ) -> Result<(LotteryRound, i64), CoreError> {
    // Find newest LotteryRound.
    let res = LotteryRound::find()
      .order_by(QuerySort::Desc("id".into()))
      .limit(1)
      .one::<LotteryRound>(redis_con)
      .await?
      .ok_or_else(|| CoreError::MissingData("Unable to get last round".into()))?;
    Ok(res)
  }

  /// Gets seed by given ID.
  ///
  /// # Error
  /// In case of database issue `CoreError` is returned.
  async fn get_seed(
    &self,
    seed_id: i64,
    redis_con: &mut Connection,
  ) -> Result<(Seed, i64), CoreError> {
    // Find seed by ID.
    let res = Seed::find()
      .filter(Filter::Condition("id".into(), RedisVal::Numeric(seed_id)))
      .one::<Seed>(redis_con)
      .await?
      .ok_or_else(|| CoreError::MissingData("Unable to get round seed".into()))?;
    Ok(res)
  }

  /// Creates new round for given seed. Model is saved as well.
  ///
  /// # Error
  /// In case of database issue `CoreError` is returned.
  async fn generate_next_round(
    &self,
    next_seed_pk: i64,
    redis_con: &mut Connection,
  ) -> Result<(LotteryRound, i64), CoreError> {
    // Create and save Round.
    let res = LotteryRound::new(next_seed_pk).db_save(redis_con).await?;
    Ok(res)
  }

  /// Gets all unaccounted bets with given round *and previous ones*.
  ///
  /// # Error
  /// In case of database issue `CoreError` is returned.
  async fn get_unaccounted_bets(
    &self,
    round_id: i64,
    redis_con: &mut Connection,
  ) -> Result<Vec<(LotteryBet, i64)>, CoreError> {
    // Find all unnacounted bets with round less or equal to given ID.
    let res = LotteryBet::find()
      .filter(Filter::AndCondition(vec![
        Filter::Condition("is_accounted".into(), RedisVal::Bool(false)),
        Filter::LessOrEqualCondition("round_id".into(), round_id),
      ]))
      .all::<LotteryBet>(redis_con)
      .await?;
    Ok(res)
  }

  /// Reveals seed and saves model.
  ///
  /// # Error
  /// In case of database issue `CoreError` is returned.
  async fn reveal_seed(&self, mut seed: Seed, redis_con: &mut Connection) -> Result<(), CoreError> {
    seed.make_visible();
    seed.db_save(redis_con).await?;
    Ok(())
  }

  /// Processes list of bets.
  ///
  /// # Error
  /// In case of database/wamp issue or processing error `CoreError` is returned.
  async fn process_bets(
    &self,
    bets: Vec<(LotteryBet, i64)>,
    redis_con: &mut Connection,
    wamp_client: &Client<'static>,
  ) -> Result<(), CoreError> {
    // To avoid unecessary queries we cache winning numbers per round ID.
    let nums_cache: HashMap<i64, [u8; 6]> = HashMap::new();

    // For every bet fetch winning numbers (or get from cache) and call processing function.
    for (bet, bet_id) in bets {
      let round_id = bet.round_id();
      let winning_nums = match nums_cache.get(&round_id) {
        Some(nums) => *nums,
        None => self.get_winning_numbers(round_id, redis_con).await?,
      };
      self
        .process_bet(bet, bet_id, &winning_nums, redis_con, wamp_client)
        .await?;
    }

    Ok(())
  }

  /// Gets winning numbers for given round.
  /// NOTE: There might be no result yet.
  ///
  /// # Error
  /// In case of database issue or missing result `CoreError` is returned.
  async fn get_winning_numbers(
    &self,
    round_id: i64,
    redis_con: &mut Connection,
  ) -> Result<[u8; 6], CoreError> {
    // Find round result and return its numbers.
    let res_opt = RoundResult::find()
      .filter(Filter::Condition(
        "round_id".into(),
        RedisVal::Numeric(round_id),
      ))
      .one::<RoundResult>(redis_con)
      .await?;
    if let Some((res, _res_pk)) = res_opt {
      return Ok(*res.numbers());
    }

    // There is no result yet, so we return error.
    Err(CoreError::MissingData(
      "Unable to find winning nums.".into(),
    ))
  }
}

impl Default for LotteryCore {
  /// Default constructor.
  fn default() -> Self {
    Self::new()
  }
}
