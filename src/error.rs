use thiserror::Error as ThisError;

/// Library error.
#[derive(ThisError, Debug)]
pub enum CoreError {
  /// Redis pool cannot be created.
  #[error("Unable to create Redis pool.")]
  CreatePoolErrorRedis(#[from] deadpool_redis::CreatePoolError),
  /// Redis connection cannot be obtained from pool.
  #[error("Unable to get Redis connection from pool.")]
  PoolErrorRedis(#[from] deadpool_redis::PoolError),
  /// WAMP pool cannot be created.
  #[error("Unable to create WAMP pool.")]
  CretePoolErrorWAMP(#[from] deadpool_wamp::CreatePoolError),
  /// WAMP client cannot be obtained from pool.
  #[error("Unable to get Redis connection from pool.")]
  PoolErrorWAMP(#[from] deadpool_wamp::PoolError),

  /// WAMP error, e.g unable to send.
  #[error("Direct WAMP error occured.")]
  ErrorWAMP(#[from] wamp_async::WampError),

  /// Model-related error, where incoming input is not valid.
  #[error("Invalid input.")]
  InvalidInput(String),
  /// Model-related error, where stored data is missing or invalid.
  #[error("Data is missing.")]
  MissingData(String),

  /// Error from ORM library.
  #[error("ActiveRecord error.")]
  ActiveRecordError(#[from] redis_orm::error::Error),
}
